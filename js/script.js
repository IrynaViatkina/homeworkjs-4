let n = +prompt("Please, enter a number: ", 0);

function fibonacciNumber(f0, f1, n){
	if(n == 0) return 0;
	if(n == 1 || n == -1) return f0;
	if(n > 0){
		for(let i = 3; i <= n; i++){
			let temp = f0 + f1;
			f0 = f1;
			f1 = temp;
		}
		return f1;
	}
	else{
		for(let i = -3; i >= n; i--){
			let temp = f0 - f1;
			f0 = f1;
			f1 = temp;
		}
		return f1;
	}
}
console.log(fibonacciNumber(1, -1, n)); // проверка